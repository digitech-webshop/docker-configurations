docker network create --subnet=172.22.0.0/16 --gateway=172.22.0.1 --opt "com.docker.network.bridge.name"="frontend_webshop-bridge" --opt "com.docker.network.bridge.enable_ip_masquerade"="true" --driver=bridge frontend_webshop-network

docker network update --dns=192.168.220.13 frontend_webshop-network

