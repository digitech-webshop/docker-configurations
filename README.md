# Konfigurations-Dokumentation

## Installation / Konfiguration Docker

### Install Docker and Compose
```bash
sudo apt install docker.io
sudo apt install docker-compose


### Git Repository clonen
Als nächstes kann unser Git-Repository geclont werden, um die entsprechenden Files auf das System zu bekommen:
```bash
git clone https://gitlab.com/digitech-webshop/docker-configurations.git
cd digitech-webshop
```

### DNS (bind) bereitstellen
Als erstes muss nun der Bind DNS gestartet werden, damit die Namensauflösung funktioniert:
```bash
sudo systemctl stop systemd-resolved
sudo systemctl disable systemd-resolved
cd dns
# # ~/docker-configurations/dns
docker-compose up -d
```

Wenn gewünscht, können nun spezifische DNS-Konfigurationen im folgenden Verzeichnis als root vorgenommen werden: /var/lib/docker/volumes/dns_bind/_data/bind/etc

Eine Kopie unserer DNS-Konfigurationen liegt ebenfalls im GitLab Repository unter dns-configs (~/docker-configurations/dns-configs).

### Traefik bereitstellen
Dazu in den Ordner namens Traefik wechseln und dort folgenden Befehl ausführen:
```bash
# ~/docker-configurations/traefik
docker-compose up -d
```
Es kann per default über https://traefik.m300.smartlearn.ch/dashboard/#/http/routers auf das Traefik Dashboard zugegriffen werden.

### Website bereitstellen
Als letzter Schritt muss nun noch das Frontend und das Backend gestartet werden. Dazu in den Ordner namens website navigieren und ebenfalls folgenden Befehl nutzen:
```bash
sudo ./setup_frontend_network.sh
# ~/docker-configurations/website
docker-compose up -d
```
Nun sollte die Website unter https://digitech.m306.smartlearn.ch erreichbar sein und das Backend unter https://digitech.m306.smartlearn.ch/api/chatbot (nur per POST).
