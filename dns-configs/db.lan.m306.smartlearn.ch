$ttl 3600
lan.m306.smartlearn.ch.	IN	SOA	ns.m306.smartlearn.ch. root.m306.smartlearn.ch. (
			2024031401
			3600
			600
			1209600
			3600 )
lan.m306.smartlearn.ch. IN      NS      ns.m306.smartlearn.ch.
vmlf1	IN A 192.168.210.1
vmlp1	IN A 192.168.210.31
vmwp1	IN A 192.168.210.10
vmls4	IN A 192.168.210.64
vmls5	IN A 192.168.210.65
